// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Author: Quincy Tse <quincy.tse@gmail.com>

package jsonh

import (
	"testing"

	"encoding/json"
	"fmt"
	"math"
	"runtime/debug"
)

var mapList = []map[string]interface{}{
	{},
	{},
	{},
	{
		"I": -1,
		"U": 10,
		"F": 1.8,
		"S": "str1",
		"B": true,
	},
	{
		"I": 11,
		"U": 103,
		"F": -1.3,
		"S": "str2",
		"B": true,
	},
	{
		"I": -99,
		"U": 222,
		"F": 101.8,
		"S": "str3",
		"B": false,
	},
	{},
}

type testType struct {
	I int
	U uint
	F float64
	S string
	B bool
}

func (t testType) ApproxEquals(o testType) bool {
	return o.I == t.I &&
		o.U == t.U &&
		math.Abs(o.F-t.F) < 1e-9 &&
		o.S == t.S &&
		o.B == t.B
}

func (t testType) ApproxEqualsMap(o map[string]interface{}) bool {
	eq := true

	switch o["I"].(type) {
	case int:
		eq = eq && (t.I == o["I"])
	case uint:
		eq = eq && (t.I == int(o["I"].(uint)))
	case float32:
		eq = eq && math.Abs(float64(t.I)-float64(o["I"].(float32))) < 1e-9
	case float64:
		eq = eq && math.Abs(float64(t.I)-o["I"].(float64)) < 1e-9
	default:
		return false
	}

	switch o["U"].(type) {
	case int:
		eq = eq && (t.U == o["U"])
	case float32:
		eq = eq && math.Abs(float64(t.U)-float64(o["U"].(float32))) < 1e-9
	case float64:
		eq = eq && math.Abs(float64(t.U)-o["U"].(float64)) < 1e-9
	default:
		return false
	}

	switch o["F"].(type) {
	case float32:
		eq = eq && math.Abs(t.F-float64(o["F"].(float32))) < 1e-9
	case float64:
		eq = eq && math.Abs(t.F-o["F"].(float64)) < 1e-9
	default:
		return false
	}

	eq = eq && t.S == o["S"]
	eq = eq && t.B == o["B"]

	return eq
}

func compareMaps(a, b map[string]interface{}) (res bool) {
	for k, v := range a {
		var v1 interface{}
		var v2 interface{}

		switch v.(type) {
		case int:
			v1 = float64(v.(int))
		case uint:
			v1 = float64(v.(uint))
		case float32:
			v1 = float64(v.(float32))
		case float64:
			v1 = v.(float64)
		default:
			v1 = v
		}

		switch b[k].(type) {
		case int:
			v2 = float64(b[k].(int))
		case uint:
			v2 = float64(b[k].(uint))
		case float32:
			v2 = float64(b[k].(float32))
		case float64:
			v2 = b[k].(float64)
		default:
			v2 = v
		}

		if x1, ok := v1.(float64); ok {
			if x2, ok := v2.(float64); !ok || math.Abs(x1-x2) > 1e-9 {
				return false
			}
		} else {
			if v1 != v2 {
				return false
			}
		}
	}

	return true
}

var structList = []testType{
	{},
	{},
	{},
	{
		I: -1,
		U: 10,
		F: 1.8,
		S: "str1",
		B: true,
	},
	{
		I: 11,
		U: 103,
		F: -1.3,
		S: "str2",
		B: true,
	},
	{
		I: -99,
		U: 222,
		F: 101.8,
		S: "str3",
		B: false,
	},
	{},
}

// Pack

func TestPackNil(t *testing.T) {
	defer panicRecover(t)

	var src []map[string]interface{}

	res, err := Pack(src)
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	}
}

// BUG(quincy) - test for Marshal/Unmarshal only tests map[string]interface{}, not generic structs

func TestPackEmpty(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[:0]

	_, err := Pack(src)
	if err == nil {
		t.Fatalf(" error expected but not returned")
	}
}

func TestPackOneEmpty(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[:1]

	res, err := Pack(src)
	if err != nil {
		t.Fatalf(" error returned: " + err.Error())
	}

	if len(res) != 1 {
		t.Fatalf(" len(res) not 1")
	}

	if res[0] != 0 {
		t.Fatalf(" row-length incorrect: " + fmt.Sprint(res[0]))
	}
}

func TestPackTwoEmpty(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[:2]

	res, err := Pack(src)
	if err != nil {
		t.Fatalf(" error returned: " + err.Error())
	}

	if len(res) != 1 {
		t.Fatalf(" len(res) not 1")
	}

	if res[0] != 0 {
		t.Fatalf(" row-length incorrect: " + fmt.Sprint(res[0]))
	}
}

func TestPackNEmpty(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[:3]

	res, err := Pack(src)
	if err != nil {
		t.Fatalf(" error returned: " + err.Error())
	}

	if len(res) != 1 {
		t.Fatalf(" len(res) not 1")
	}

	if res[0] != 0 {
		t.Fatalf(" row-length incorrect: " + fmt.Sprint(res[0]))
	}
}

func TestPackHet1(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[2:4]

	_, err := Pack(src)
	if err == nil {
		t.Fatalf(" expect error but none was returned")
	}
}

func TestPackHet2(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[5:7]

	_, err := Pack(src)
	if err == nil {
		t.Fatalf(" expect error but none was returned")
	}
}

func TestPackOne(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:4]

	res, err := Pack(src)
	if err != nil {
		t.Fatalf(" error returned: " + err.Error())
	}

	if len(res) != 1+(len(src)+1)*len(src[0]) {
		t.Fatalf(" len(res) is %d, not %d.", len(res), (len(src)+1)*(len(src[0])+1))
	}

	if res[0] != len(src[0]) {
		t.Fatalf(" row-length incorrect: %d instead if %d", res[0], len(src[0]))
	}

	kmap := make(map[string]int)
	for i := 1; i < 6; i++ {
		k, ok := res[i].(string)
		if !ok {
			t.Fatalf(" name of key at %d(%s) is not a string.", i-1, fmt.Sprint(res[i]))
		}
		kmap[k] = i - 1
	}

	for _, k := range []string{"I", "U", "F", "S", "B"} {
		if _, found := kmap[k]; !found {
			t.Fatalf(" key %s not found.", k)
		}
	}

	for i, _ := range src {
		currRow := res[1+res[0].(int)*(i+1) : 1+res[0].(int)*(i+2)]
		for k, j := range kmap {
			if currRow[j] != src[i][k] {
				t.Fatalf(" value of row %d key %s differs: src[%d][%s]=%s vs row value %s",
					i, k, i, k, src[i][k], currRow[j])
			}
		}
	}
}

func TestPackTwo(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:5]

	res, err := Pack(src)
	if err != nil {
		t.Fatalf(" error returned: " + err.Error())
	}

	if len(res) != 1+(len(src)+1)*len(src[0]) {
		t.Fatalf(" len(res) is %d, not %d.", len(res), (len(src)+1)*(len(src[0])+1))
	}

	if res[0] != len(src[0]) {
		t.Fatalf(" row-length incorrect: %d instead if %d", res[0], len(src[0]))
	}

	kmap := make(map[string]int)
	for i := 1; i < 6; i++ {
		k, ok := res[i].(string)
		if !ok {
			t.Fatalf(" name of key at %d(%s) is not a string.", i-1, fmt.Sprint(res[i]))
		}
		kmap[k] = i - 1
	}

	for _, k := range []string{"I", "U", "F", "S", "B"} {
		if _, found := kmap[k]; !found {
			t.Fatalf(" key %s not found.", k)
		}
	}

	for i, _ := range src {
		currRow := res[1+res[0].(int)*(i+1) : 1+res[0].(int)*(i+2)]
		for k, j := range kmap {
			if currRow[j] != src[i][k] {
				t.Fatalf(" value of row %d key %s differs: src[%d][%s]=%s vs row value %s",
					i, k, i, k, src[i][k], currRow[j])
			}
		}
	}
}

func TestPackN(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:6]

	res, err := Pack(src)
	if err != nil {
		t.Fatalf(" error returned: " + err.Error())
	}

	if len(res) != 1+(len(src)+1)*len(src[0]) {
		t.Fatalf(" len(res) is %d, not %d.", len(res), (len(src)+1)*(len(src[0])+1))
	}

	if res[0] != len(src[0]) {
		t.Fatalf(" row-length incorrect: %d instead if %d", res[0], len(src[0]))
	}

	kmap := make(map[string]int)
	for i := 1; i < 6; i++ {
		k, ok := res[i].(string)
		if !ok {
			t.Fatalf(" name of key at %d(%s) is not a string.", i-1, fmt.Sprint(res[i]))
		}
		kmap[k] = i - 1
	}

	for _, k := range []string{"I", "U", "F", "S", "B"} {
		if _, found := kmap[k]; !found {
			t.Fatalf(" key %s not found.", k)
		}
	}

	for i, _ := range src {
		currRow := res[1+res[0].(int)*(i+1) : 1+res[0].(int)*(i+2)]
		for k, j := range kmap {
			if currRow[j] != src[i][k] {
				t.Fatalf(" value of row %d key %s differs: src[%d][%s]=%s vs row value %s",
					i, k, i, k, src[i][k], currRow[j])
			}
		}
	}
}

// Unpack - assumes pack works

func TestUnpackNil(t *testing.T) {
	defer panicRecover(t)

	var src []interface{}

	res, err := Unpack(src)
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	}
}

func TestUnpackEmpty(t *testing.T) {
	defer panicRecover(t)

	src := make([]interface{}, 0)

	res, err := Unpack(src)
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	}
}

func TestUnpackOneEmpty(t *testing.T) {
	defer panicRecover(t)

	src := []interface{}{0}

	res, err := Unpack(src)
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	}
}

func TestUnpackOne(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:4]

	packed, err := Pack(src)
	if err != nil {
		t.Fatal(" Pack failed:", err)
	}

	res, err := Unpack(packed)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		for k, v := range m {
			if v != res[i][k] {
				t.Fatalf(" values not match: src[%d][%s]=%s, res[%d][%s]=%s",
					i, k, fmt.Sprint(src[i][k]), i, k, fmt.Sprint(res[i][k]))
			}
		}
	}
}

func TestUnpackTwo(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:5]

	packed, err := Pack(src)
	if err != nil {
		t.Fatal(" Pack failed:", err)
	}

	res, err := Unpack(packed)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		for k, v := range m {
			if v != res[i][k] {
				t.Fatalf(" values not match: src[%d][%s]=%s, res[%d][%s]=%s",
					i, k, fmt.Sprint(src[i][k]), i, k, fmt.Sprint(res[i][k]))
			}
		}
	}
}

func TestUnpackN(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:6]

	packed, err := Pack(src)
	if err != nil {
		t.Fatal(" Pack failed:", err)
	}

	res, err := Unpack(packed)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		for k, v := range m {
			if v != res[i][k] {
				t.Fatalf(" values not match: src[%d][%s]=%s, res[%d][%s]=%s",
					i, k, fmt.Sprint(src[i][k]), i, k, fmt.Sprint(res[i][k]))
			}
		}
	}
}

func TestUnpackFaulty(t *testing.T) {
	defer panicRecover(t)

	src := []interface{}{"a", "b"}

	res, err := Unpack(src)
	if err == nil {
		t.Error(" accepted invalid list and produced output:", res)
	}
}

// Marshal - assumes Pack and Unpack works

func TestMarshalNil(t *testing.T) {
	defer panicRecover(t)

	var src []testType

	res, err := Marshal(src)
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	}
}

func TestMarshalEmpty(t *testing.T) {
	defer panicRecover(t)

	var src = structList[:0]

	_, err := Marshal(src)
	if err == nil {
		t.Fatalf(" error expected but not returned.")
	}
}

func TestMarshalOne(t *testing.T) {
	defer panicRecover(t)

	var src = structList[3:4]

	js, err := Marshal(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var obj []interface{}
	err = json.Unmarshal(js, &obj)
	if err != nil {
		t.Fatalf(" json from Marshal cannot be json.Umuarshal(): %s", err.Error())
	}

	res, err := Unpack(obj)
	if err != nil {
		t.Fatalf(" Unmarshal() list cannot be Unpacked(): %s", err.Error())
	}

	for i, m := range src {
		if !m.ApproxEqualsMap(res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestMarshalTwo(t *testing.T) {
	defer panicRecover(t)

	var src = structList[3:5]

	js, err := Marshal(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var obj []interface{}
	err = json.Unmarshal(js, &obj)
	if err != nil {
		t.Fatalf(" json from Marshal cannot be json.Umuarshal(): %s", err.Error())
	}

	res, err := Unpack(obj)
	if err != nil {
		t.Fatalf(" Unmarshal() list cannot be Unpacked(): %s", err.Error())
	}

	for i, m := range src {
		if !m.ApproxEqualsMap(res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestMarshalN(t *testing.T) {
	defer panicRecover(t)

	var src = structList[3:6]

	js, err := Marshal(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var obj []interface{}
	err = json.Unmarshal(js, &obj)
	if err != nil {
		t.Fatalf(" json from Marshal cannot be json.Umuarshal(): %s", err.Error())
	}

	res, err := Unpack(obj)
	if err != nil {
		t.Fatalf(" Unmarshal() list cannot be Unpacked(): %s", err.Error())
	}

	for i, m := range src {
		if !m.ApproxEqualsMap(res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestMarshalNonList(t *testing.T) {
	defer panicRecover(t)

	for _, s := range []interface{}{"10", 4.1, mapList[3]} {
		if _, err := Marshal(s); err == nil {
			t.Errorf(" Marshal() not returning error for: %s", fmt.Sprint(s))
		}
	}
}

// Unmarshal

func TestUnmarshalNil(t *testing.T) {
	defer panicRecover(t)

	var src []byte
	var res []testType
	var jsonObj []testType

	jsonErr := json.Unmarshal(src, &jsonObj)

	err := Unmarshal(src, &res)
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	} else if jsonErr != nil && err.Error() != jsonErr.Error() {
	  t.Fatalf(" did not forward json module error correctly. json:%s jsonh:%s", jsonErr, err)
  } else if _,ok := err.(*NonJsonhError); jsonErr == nil && !ok {
    t.Fatalf(" expect NonJsonhError but a different error was returned: %s", err.Error())
  }
}

func TestUnmarshalOne(t *testing.T) {
	defer panicRecover(t)

	var src = structList[3:4]

	js, err := Marshal(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var res []testType
	err = Unmarshal(js, &res)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		if !m.ApproxEquals(res[i]) {
			t.Fatalf(" values not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestUnmarshalTwo(t *testing.T) {
	defer panicRecover(t)

	var src = structList[3:5]

	js, err := Marshal(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var res []testType
	err = Unmarshal(js, &res)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		if !m.ApproxEquals(res[i]) {
			t.Fatalf(" values not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestUnmarshalN(t *testing.T) {
	defer panicRecover(t)

	var src = structList[3:6]

	js, err := Marshal(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var res []testType
	err = Unmarshal(js, &res)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		if !m.ApproxEquals(res[i]) {
			t.Fatalf(" values not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestUnmarshalInvalid(t *testing.T) {
	defer panicRecover(t)

	for _, s := range [][]byte{[]byte("abc"), []byte("{}"), []byte("[\"a\"]"), []byte("{}"), []byte("[[]]"), []byte("[2]")} {
    var jsonObj interface{}
    jsonErr := json.Unmarshal(s, &jsonObj)

    var v interface{}
    err := Unmarshal(s, &v)
    if err == nil {
      t.Errorf(" expect error but none was returned: src:%s", string(s))
      t.Fatalf(" v: " + fmt.Sprint(v))
    } else if jsonErr != nil && err.Error() != jsonErr.Error() {
      t.Fatalf(" did not forward json module error correctly. json:%s jsonh:%s", jsonErr, err)
    } else if _,ok := err.(*NonJsonhError); jsonErr == nil && !ok {
      t.Fatalf(" expect NonJsonhError but a different error was returned: %s", err.Error())
    }
	}
}

func TestUnmarshalNormalJson(t *testing.T) {
	defer panicRecover(t)

	var src = structList[3:6]

  js, jsonErr := json.Marshal(src)
	if jsonErr != nil {
		t.Fatalf(" error returned: %s", jsonErr.Error())
	}

	var res []testType
  err := Unmarshal(js, &res)
  if err == nil {
    t.Errorf(" expect error but none was returned: src:%s", string(js))
    t.Fatalf(" res: " + fmt.Sprint(res))
  } else if jsonErr != nil && err.Error() != jsonErr.Error() {
    t.Fatalf(" did not forward json module error correctly. json:%s jsonh:%s", jsonErr, err)
  } else {
    if _,ok := err.(*NonJsonhError); jsonErr == nil && !ok {
      t.Fatalf(" expect NonJsonhError but a different error was returned: %s", err.Error())
    } else {
      if len(src) != len(res) {
        t.Fatalf(" result length and source length not same when Unmarshalling standard JSON")
      } else {
        for i,v := range src {
          if !v.ApproxEquals(res[i]) {
            t.Fatalf(" result list not match when Unmarshalling standard JSON")
          }
        }
      }
    }
  }
}

// Dumps

func TestDumpsNil(t *testing.T) {
	defer panicRecover(t)

	var src []map[string]interface{}

	res, err := Dumps(src)
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	}
}

func TestDumpsEmpty(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[:0]

	_, err := Dumps(src)
	if err == nil {
		t.Fatalf(" error expected but not returned")
	}
}

func TestDumpsOne(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:4]

	js, err := Dumps(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var obj []interface{}
	err = json.Unmarshal([]byte(js), &obj)
	if err != nil {
		t.Fatalf(" json from Dumps cannot be json.Umuarshal(): %s", err.Error())
	}

	res, err := Unpack(obj)
	if err != nil {
		t.Fatalf(" Unmarshal() list cannot be Unpacked(): %s", err.Error())
	}

	for i, m := range src {
		if !compareMaps(m, res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestDumpsTwo(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:5]

	js, err := Dumps(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var obj []interface{}
	err = json.Unmarshal([]byte(js), &obj)
	if err != nil {
		t.Fatalf(" json from Dumps cannot be json.Umuarshal(): %s", err.Error())
	}

	res, err := Unpack(obj)
	if err != nil {
		t.Fatalf(" Unmarshal() list cannot be Unpacked(): %s", err.Error())
	}

	for i, m := range src {
		if !compareMaps(m, res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestDumpsN(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:6]

	js, err := Dumps(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	var obj []interface{}
	err = json.Unmarshal([]byte(js), &obj)
	if err != nil {
		t.Fatalf(" json from Dumps cannot be json.Umuarshal(): %s", err.Error())
	}

	res, err := Unpack(obj)
	if err != nil {
		t.Fatalf(" Unmarshal() list cannot be Unpacked(): %s", err.Error())
	}

	for i, m := range src {
		if !compareMaps(m, res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

// Loads

func TestLoadsNil(t *testing.T) {
	defer panicRecover(t)

	var src []byte

	res, err := Loads(string(src))
	if err == nil {
		t.Errorf(" expect error but none was returned")
		t.Fatalf(" res: " + fmt.Sprint(res))
	}
}

func TestLoadsOne(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:4]

	js, err := Dumps(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	res, err := Loads(js)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		if !compareMaps(m, res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestLoadsTwo(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:5]

	js, err := Dumps(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	res, err := Loads(js)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		if !compareMaps(m, res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestLoadsN(t *testing.T) {
	defer panicRecover(t)

	var src = mapList[3:6]

	js, err := Dumps(src)
	if err != nil {
		t.Fatalf(" error returned: %s", err.Error())
	}

	res, err := Loads(js)
	if err != nil {
		t.Fatalf(" unexpected error: %s", err.Error())
	}

	for i, m := range src {
		if !compareMaps(m, res[i]) {
			t.Fatalf(" elements not match: src[%d]=%s, res[%d]=%s",
				i, fmt.Sprint(m), i, fmt.Sprint(res[i]))
		}
	}
}

func TestLoadsInvalid(t *testing.T) {
	defer panicRecover(t)

	for _, s := range []string{"abc", "{}", "[\"a\"]", "{}", "[[]]", "[2]"} {
		if _, err := Loads(s); err == nil {
			t.Errorf(" did not return error on input \"%s\"", string(s))
		}
	}
}

func panicRecover(t *testing.T) {
	if r := recover(); r != nil {
		t.Errorf("paniced: %s", r)
		t.Fatal(string(debug.Stack()))
	}
}
