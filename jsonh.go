// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Author: Quincy Tse <quincy.tse@gmail.com>

/*
The jsonh package linearises and compacts a list of homogeneous objects according
to the JSONH specifications on https://github.com/WebReflection/JSONH. This package
exposes an API somilar to the standard json package and respects the "json:" tags
as per the json package.

Note: if certain "jsob:" tags causes some keys to be optionally available, jsonh
will recognise this as a heterogeneous map and will refuse to compact.

Functions in this package returns error defiend both in this package and in the
standard json package. Errors resulting from json operations will return errprs
from the json package, whereas JSONH-specific errors are defined in this package.

Loads() and Dumps() are retained from the original Python2 code, but may be
deprecated in the future in favaour of the standard like interface.
*/
package jsonh

import (
	"fmt"
	"reflect"
	"strconv"

	"encoding/json"
)

// An UnsupportedListError is returned by functions that packs/unpacks a JSONH-packed
// objects when attempting to decode non-JSONH-packed list.
type UnsupportedListError struct {
	Str string
}

func (e *UnsupportedListError) Error() string {
	return "jsonh: unsupported list: " + e.Str
}

// An UnsupportedTypeError is returned by Marshal when attempting
// to encode an unsupported value type.
type UnsupportedTypeError struct {
	Type reflect.Type
}

func (e *UnsupportedTypeError) Error() string {
	return "jsonh: unsupported type: " + e.Type.String()
}

// An HeterogeneousListError is returned by Marshal when attempting
// to encode an unsupported value type.
type HeterogeneousListError struct {
	Item int
}

func (e *HeterogeneousListError) Error() string {
	return "jsonh: different item at position " + strconv.Itoa(e.Item)
}

// An NonJsonhError is returned by Unmarshal when a non-JSONH-packed
// objectss processed. Returning this error does not imply the result is
// invalid.
type NonJsonhError struct {
	Js []byte
	Err error
}

func (e *NonJsonhError) Error() string {
	return "jsonh: not a JSONH-packed object: " + string(e.Js)
}

// Takes a slice of maps and marshals it into a JSONH string.
// Equivalent to dump() in the original python2 code
func Dumps(obj []map[string]interface{}) (res string, err error) {
	var packed interface{}
	packed, err = Pack(obj)
	if err != nil {
		return
	}

	var b []byte
	b, err = json.Marshal(packed)
	res = string(b)
	return
}

// Similar to Dumps, but uses the function names in the encoding/json package
// Go-version of the original dump() function, using the Go function names
func Marshal(v interface{}) (res []byte, err error) {
	if vk := reflect.ValueOf(v).Kind(); vk != reflect.Array && vk != reflect.Slice {
		err = &UnsupportedTypeError{reflect.TypeOf(v)}
		return
	}

	l, ok := v.([]interface{})
	if !ok {
		x := reflect.ValueOf(v)
		l = make([]interface{}, x.Len())
		for i := 0; i < x.Len(); i++ {
			l[i] = x.Index(i).Interface()
		}
	}

	var js []byte
	js, err = json.Marshal(l)
	if err != nil {
		return
	}

	var lm []map[string]interface{}
	err = json.Unmarshal(js, &lm)
	if err != nil {
		return
	}

	var packed interface{}
	packed, err = Pack(lm)
	if err != nil {
		return
	}

	res, err = json.Marshal(packed)
	return
}

// Loads a JSONH string (a flat array) into a a slice of maps
// Equivalent to loads() in the original python2 code
func Loads(str string) (res []map[string]interface{}, err error) {
	var l []interface{}
	err = json.Unmarshal([]byte(str), &l)
	if err != nil {
		return
	}
	res, err = Unpack(l)
	return
}

// Similar to Dumps, but uses the function names in the encoding/json package
// Go-version of the original dump() function, using the Go function names
// This function acts as a stand-in replacement for json.Unmarshal, and returns
// NonJsonhError when a valid json-encoded object is passed in. The results v
// is still a valid decoding of the object under JSON but not under JSONH.
func Unmarshal(str []byte, v interface{}) (err error) {
	var l []interface{}
  err2 := json.Unmarshal(str, &l)
	if err2 != nil {
	  // input not a list - cannot possibly be JSONH
	  // revert to standard json.Unmarshal
    err = json.Unmarshal(str, v)
    if err != nil {
      // Not even valid json
      return
    }

		return &NonJsonhError{str, err2}
	}

	var m interface{}
	m, err2 = Unpack(l)
	if err2 != nil {
    err = json.Unmarshal(str, v)
    if err != nil {
      // Not even valid json
      return
    }

		return &NonJsonhError{str, err2}
	}

	str, err = json.Marshal(m)
	if err != nil {
	  panic(err)
	}

	return json.Unmarshal(str, v)
}

// Linearise a homongenous slice of maps into a JSONH-compatible slice
// Equivalent to pack() in the original python2 code
func Pack(dict_list []map[string]interface{}) (res []interface{}, err error) {
	if dict_list == nil {
		res = nil
		err = &UnsupportedListError{"nil value is not supported as a list"}
		return
	}

	tlen := len(dict_list)
	if tlen == 0 {
		err = &UnsupportedListError{"0-length list not supported - cannot determine the keys"}
		return
	}

	rlen := len(dict_list[0])
	// #keys + (#records + key row)*(#keys per row)
	res = make([]interface{}, 1+(1+tlen)*rlen)
	res[0] = rlen

	klist := res[1 : rlen+1]
	offset := 0
	for k, _ := range dict_list[0] {
		klist[offset] = k
		offset++
	}

	offset = rlen + 1
	for id, r := range dict_list {
		if len(r) != rlen {
			err = &HeterogeneousListError{id}
			return
		}
		currRow := res[offset : offset+rlen]

		for i, k := range klist {
			var found bool
			if currRow[i], found = r[k.(string)]; !found {
				err = &HeterogeneousListError{i}
				return
			}

		}

		offset += rlen
	}

	return
}

// Converts a JSONH-copatible slice into a homongenous slice of maps
// Equivalent to unpack() in the original python2 code
func Unpack(hlist []interface{}) (res []map[string]interface{}, err error) {
	if len(hlist) < 1 {
		var js []byte
		js, err = json.Marshal(hlist)
		if err != nil {
			return
		}
		err = &UnsupportedListError{string(js)}
		return
	}

	var rlen int
	switch hlist[0].(type) {
	case int:
		rlen = hlist[0].(int)
	case float32:
		rlen = int(hlist[0].(float32))
	case float64:
		rlen = int(hlist[0].(float64))
	default:
		err = &UnsupportedListError{"invalid list - 1st value not an integer: " + fmt.Sprint(hlist[0]) + " " + reflect.TypeOf(hlist[0]).String()}
		return
	}

	if rlen == 0 {
		err = &UnsupportedListError{"do not support table with 0 columns - cannot deduce number of rows: " + fmt.Sprint(hlist)}
		return
	}

	if (len(hlist)-1)%rlen != 0 {
		// Not a whole number of rows
		err = &UnsupportedListError{"length not (integer multiple of row length) + 1. list length:" + strconv.Itoa(len(hlist)) + ", row length:" + strconv.Itoa(rlen)}
		return
	}

	klist := hlist[1 : 1+rlen]
	tlen := (len(hlist)-1)/rlen - 1
	if tlen < 0 {
		err = &UnsupportedListError{"number of rows is negative" + strconv.Itoa(len(hlist)) + ", row length:" + strconv.Itoa(rlen)}
		return
	}

	data := hlist[1+rlen:]

	res = make([]map[string]interface{}, tlen)

	for i := 0; i < tlen; i++ {
		currRow := data[i*rlen : i*rlen+rlen]
		res[i] = make(map[string]interface{})

		for j, k := range klist {
			res[i][k.(string)] = currRow[j]
		}
	}

	return
}
