// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Author: Quincy Tse <quincy.tse@gmail.com>

package jsonh

import (
	"testing"

	"bytes"
)

var testList = []testType{
	{
		I: -1,
		U: 10,
		F: 1.8,
		S: "str1",
		B: true,
	},
	{
		I: 11,
		U: 103,
		F: -1.3,
		S: "str2",
		B: true,
	},
	{
		I: -99,
		U: 222,
		F: 101.8,
		S: "str3",
		B: false,
	},
}

func TestEncoder(t *testing.T) {
	for i := 1; i <= len(testList); i++ {
		var buf bytes.Buffer
		enc := NewEncoder(&buf)
		err := enc.Encode(testList[:i])
		if err != nil {
			t.Fatal(err)
		}

		var res []testType
		err = Unmarshal(buf.Bytes(), &res)
		if err != nil {
			t.Fatal(err)
		}
		for j, s := range testList[:i] {
			if !s.ApproxEquals(res[j]) {
				t.Errorf(" item %d does not match: %s vs %s", j, s, res[j])
			}
		}
	}
}

func TestDecoder(t *testing.T) {
	for i := 1; i <= len(testList); i++ {
		b, err := Marshal(testList[:i])
		if err != nil {
			t.Fatal(err)
		}

		buf := bytes.NewBuffer(b)
		dec := NewDecoder(buf)

		var res []testType
		err = dec.Decode(&res)
		if err != nil {
			t.Fatal(err)
		}
		for j, s := range testList[:i] {
			if !s.ApproxEquals(res[j]) {
				t.Errorf(" item %d does not match: %s vs %s", j, s, res[j])
			}
		}
	}
}
