// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Author: Quincy Tse <quincy.tse@gmail.com>

package jsonh

import (
	"io"

	"encoding/json"
)

// A Decoder reads and decodes JSONH objects from an input stream.
type Decoder struct {
	d *json.Decoder
}

// NewDecoder returns a new decoder that reads from r.
//
// The decoder introduces its own buffering and may
// read data from r beyond the JSON values requested.
func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{d: json.NewDecoder(r)}
}

// Decode reads the next JSONH-encoded list from its
// input and stores it in the value pointed to by v.
func (dec *Decoder) Decode(v interface{}) error {
	var temp interface{}
	// Use json.Decoder to retrieve next value from stream
	err := dec.d.Decode(&temp)
	if err != nil {
		return err
	}

	// Marshal it back up then nmarshal using jsonh.Unmarshal()
	js, err := json.Marshal(temp)
	if err != nil {
		return err
	}

	return Unmarshal(js, v)
}

// An Encoder writes JSONH lists to an output stream.
type Encoder struct {
	w io.Writer
}

// NewEncoder returns a new encoder that writes to w.
func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

// Encode writes the JSONH encoding of v to the connection.
func (enc *Encoder) Encode(v interface{}) error {
	str, err := Marshal(v)
	if err != nil {
		return err
	}

	// Terminate each value with a newline.
	// This makes the output look a little nicer
	// when debugging, and some kind of space
	// is required if the encoded value was a number,
	// so that the reader knows there aren't more
	// digits coming.
	_, err = enc.w.Write([]byte("\n"))
	if err != nil {
		return err
	}

	_, err = enc.w.Write(str)
	return err
}
